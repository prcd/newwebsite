<%-- 
    Document   : nav
    Created on : Apr 4, 2014, 10:15:25 PM
    Author     : adamgray
--%>

<div id="nav">
    <ul>
        <a href="index.jsp"><li>Home</li></a>
        <a href="forsale.jsp"><li>For Sale</li></a>
        <a href="forrent.jsp"><li>For Rent</li></a>
        <a href="#"><li>Blog</li></a>
        <a href="contact.jsp"><li>Contact</li></a>
    </ul>
    <ul style="display: inline; float: right; margin-top: -28px; ">
        <a href="signin.jsp"><li>Sign In</li></a>
        <a href="#"><li>My Account</li></a>
    </ul>
</div>