<%-- 
    Document   : ListOfProperties
    Created on : 13-Mar-2014, 11:57:43
    Author     : Stu
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="Classes.*" %>
<jsp:useBean id="db" scope="page" class="Classes.DBConnect" />

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Property manager</title>
    </head>
    <body bgcolor="D0CFFD"><font face="comic sans ms"/>
        <h3>List of Properties</h3><hr>
        <table border=0 width="75%">

        <%
            db.createConnection();
            PropertyList list = db.allProperties();
            db.closeConnection();
            for (int pos = 0; pos < list.size(); pos ++) {
                Property property = list.retrievePropertyAt(pos);
        %>

        <br><%= property.getHouseNo()%>  <%= property.getAddressLine1()%>  <%= property.getCounty()%>


            <%
                } // end for
            %>

        </table>
        <br><a href="ViewCart.jsp">View your shopping cart</a><br>
    </body>
</html>
