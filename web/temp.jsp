<%-- 
    Document   : newjsp
    Created on : Apr 3, 2014, 7:08:41 PM
    Author     : adamgray
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main.css"/>
        <title>Search Results</title>
    </head>
    <body>
        <jsp:include page="includes/nav.jsp" />
        <jsp:include page="includes/header.jsp" />
        <div id="contentContainer">
            <br/>
            <div id="content">
                <h1>Search Results</h1>
            </div>
            <br/>
        </div>
        <jsp:include page="includes/footer.jsp" />
    </body>
</html>
