<%-- 
    Document   : newjsp
    Created on : Apr 3, 2014, 7:08:41 PM
    Author     : adamgray
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@page import="Classes.*" %>
<jsp:useBean id="db" scope="page" class="Classes.DBConnect"/>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main.css"/>
        <title>Search Results</title>
    </head>
    <body>
        <jsp:include page="includes/nav.jsp" />
        <jsp:include page="includes/header.jsp" />
        <div id="contentContainer">
            <br/>
            <div id="contentSide">
                <div class="propertyForm">
                    <span class="normal-bold">Refine your search</span><br/><br/>
                    <form>
                        Location: <input></input><br/><br/>
                        Beds: <select>
                            <option value="nomin">No Min</option>
                            <option value="studio">Studio</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                        <select>
                            <option value="nomax">No Max</option>
                            <option value="studio">Studio</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </form>
                </div>
            </div>
            <div id="content">
                <span style="margin-left: 50px;"><h1>Search Results for &quotINSERT USER QUERY&quot</h1></span>
                <div id="listOrder">
                    <span>Showing <span class="normal-bold">12 </span> Results.</span>
                    <span style="margin-left: 390px;">Sort by:</span>
                    <select>
                        <option value="mostrecent">Most Recent</option>
                        <option value="highestprice">Highest Price</option>
                        <option value="lowestprice">Lowest Price</option>
                    </select>
                </div>
                <%
                    
                    // Start of foreach
                    
                %>
                <div class="listItem">
                    <div class="propertyImage">
                        <img src="images/property/1/1.jpg" width="200" height="125"/>
                    </div>
                    <div class="propertyDetails">
                        <a class="list-address" href="#">7 Baker Street, Mutley, Plymouth</a><br/>
                        <span class="list-price">£300,00</span><br/>
                        <span class="list-details">3 Bedrooms</span><br/>
                        <span><a href="#">More Details</a></span>
                    </div>
                </div>
                <div class="listItem">
                    <div class="propertyImage">
                        <img src="images/property/1/1.jpg" width="200" height="125"/>
                    </div>
                    <div class="propertyDetails">
                        <a class="list-address" href="#">7 Baker Street, Mutley, Plymouth</a><br/>
                        <span class="list-price">£300,00</span><br/>
                        <span class="list-details">3 Bedrooms</span><br/>
                        <span><a href="#">More Details</a></span>
                    </div>
                </div>
                <div class="listItem">
                    <div class="propertyImage">
                        <img src="images/property/1/1.jpg" width="200" height="125"/>
                    </div>
                    <div class="propertyDetails">
                        <a class="list-address" href="#">7 Baker Street, Mutley, Plymouth</a><br/>
                        <span class="list-price">£300,00</span><br/>
                        <span class="list-details">3 Bedrooms</span><br/>
                        <span><a href="#">More Details</a></span>
                    </div>
                </div>
                <%
                    
                    // End of foreach
                    
                %>
            </div>
            <br/>
        </div>
        <jsp:include page="includes/footer.jsp" />
    </body>
</html>
