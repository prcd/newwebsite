<%-- 
    Document   : newjsp
    Created on : Apr 3, 2014, 7:08:41 PM
    Author     : adamgray
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main.css"/>
        <title>Search Results</title>
    </head>
    <body>
        <jsp:include page="includes/nav.jsp" />
        <jsp:include page="includes/header.jsp" />
        <div id="contentContainer">
            <br/>
            <div id="content">
                <h1 style="margin-left: 30px;">Create an account here.</h1>
                    <div class="clear-container" style="margin-left: 30px; display: inline-block;">
                    <form name="register" action="#" method="post">
                        <span class="normal-bold">First Name</span><br/><input type="text" name="firstname" size="30" style="margin-left: -1px;"/><br/><br/>
                        <span class="normal-bold">Surname</span><br/><input type="text" name="surname" size="30" style="margin-left: -1px;"/><br/><br/>
                        <span class="normal-bold">Email </span><br/><input type="text" name="email" size="50" style="margin-left: -1px;"/><br/><br/>
                        <span class="normal-bold">Phone Number </span><br/><input type="text" name="phonenumber" size="30" style="margin-left: -1px;"/><br/><br/>
                        <span class="normal-bold">Password </span><br/><input type="password" name="password" size="50" style="margin-left: -1px;"/><br/><br/>
                        <span class="normal-bold">Retype Password </span><br/><input type="password" name="retypepassword" size="50" style="margin-left: -1px;"/><br/><br/>
                        <button class="button-green" type="submit" style="width: 150px;">Create My Account</button>
                    </form>
                </div>
            </div>
            <br/>
        </div>
        <jsp:include page="includes/footer.jsp" />
    </body>
</html>
