<%-- 
    Document   : newjsp
    Created on : Apr 3, 2014, 7:08:41 PM
    Author     : adamgray
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main.css"/>
        <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="js/jquery.fancybox.js?v=2.1.5"></script>
	<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css?v=2.1.5" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/jquery.fancybox-buttons.css?v=1.0.5" />
	<script type="text/javascript" src="js/jquery.fancybox-buttons.js?v=1.0.5"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $(".fancybox").fancybox();
                $('.fancybox-buttons').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});
            });
        </script>
        <title>Search Results</title>
    </head>
    <body>
        <jsp:include page="includes/nav.jsp" />
        <jsp:include page="includes/header.jsp" />
        <div id="contentContainer">
            <br/>
            <div id="content" style="margin: 20px;">
                <a href="#">< back to search results</a><br/><br/>
                <span class="title">House Address</span><br/>
                <span class="price">£210,000</span>
                <span class="subtitle"> | 3 Bedrooms | For Sale</span><bbr/>
                <div>
                    <a href="#" class="button-green-property">Photos</a>
                    <a href="property.jsp#viewing" class="button-green-property">Arrange Viewing</a>
                    <a href="#" class="button-green-property">Send to friend</a>
                    <a href="#" class="button-green-property">Add to favourites</a>
                </div>
                <div>
                    <img src="images/property/1/1.jpg" width="700" height="450"/><br/>
                    <a class="fancybox-buttons" data-fancybox-group="button" href="images/property/1/1.jpg">
                        <img src="images/property/1/1.jpg" width="100" height="75" alt="" />
                    </a>
                    <a class="fancybox-buttons" data-fancybox-group="button" href="images/property/1/2.jpg">
                        <img src="images/property/1/2.jpg" width="100" height="75" alt="" />
                    </a>
                    <a class="fancybox-buttons" data-fancybox-group="button" href="images/property/1/3.jpg">
                        <img src="images/property/1/3.jpg" width="100" height="75" alt="" />
                    </a>
                    <a class="fancybox-buttons" data-fancybox-group="button" href="images/property/1/4.jpg">
                        <img src="images/property/1/4.jpg" width="100" height="75" alt="" />
                    </a>
                    <a class="fancybox-buttons" data-fancybox-group="button" href="images/property/1/5.jpg">
                        <img src="images/property/1/5.jpg" width="100" height="75" alt="" />
                    </a>
                </div>
                <br/>
               
                <span class="subheading">ABOUT THIS PROPERTY</span>
                <div class="clear-container" width="500px;">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ultrices luctus nisl vel sodales. Ut sed feugiat arcu. Ut suscipit tellus eget sem vulputate, consequat venenatis elit ultrices. Morbi sed arcu a ligula volutpat porttitor vel at nisl. Nam sit amet consequat felis, sed auctor lacus. Vestibulum gravida porta malesuada. Aenean feugiat vulputate varius. Aliquam erat volutpat. Mauris nec convallis massa, nec fermentum risus. Praesent vel eros vitae justo rhoncus mollis vitae sed eros. Sed hendrerit ullamcorper lectus, in rhoncus est.
                </div>
                <span id="viewing" class="subheading">ARRANGE VIEWING</span>
                <div class="propertyForm">
                    <form name="request" action="#" method="post">
                        <span class="normal-bold">Name</span><br/><input type="text" name="name" size="30" style="margin-left: -1px;"/><br/><br/>
                        <span class="normal-bold">Email</span><br/><input type="text" name="email" size="30" style="margin-left: -1px;"/><br/><br/>
                        <span class="normal-bold">Message</span><br/><textarea type="text" name="firstname" size="30" placeholder="Please enter any useful information i.e. times availabile for viewings, " style="margin-left: -1px; width: 400px; max-width: 400px; height: 150px; max-height: 150px;"></textarea><br/><br/>
                        <button class="button-green" style="width:125px;">Send Message</button>
                    </form>
                </div>
            </div>
            <br/>
        </div>
        <jsp:include page="includes/footer.jsp" />
    </body>
</html>
