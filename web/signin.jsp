<%-- 
    Document   : newjsp
    Created on : Apr 3, 2014, 7:08:41 PM
    Author     : adamgray
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main.css"/>
        <title>Sign in or Register at PRDCD</title>
    </head>
    <body>
        <jsp:include page="includes/nav.jsp" />
        <jsp:include page="includes/header.jsp" />
        <div id="contentContainer">
            <br/>
            <div id="content">
                <h1 style="margin-left: 30px;">Please Sign in or create an account.</h1>
                <div class="clear-container" style="margin-left: 30px; display: inline-block;">
                    <form name="signin" action="#" method="post">
                        <span class="normal-bold">Email </span><br/><input type="text" name="email" size="50" style="margin-left: -1px;"/><br/><br/>
                        <span class="normal-bold">Password </span><br/><input type="password" name="password" size="50" style="margin-left: -1px;"/><br/>
                        <a href="#">forgotten password?</a><br/><br/>
                        <button class="button-green" type="submit">Sign In</button>
                        <span> or </span><a href="register.jsp">Create a new account.</a>
                    </form>
                </div>
            </div>
            <br/>
        </div>
        <jsp:include page="includes/footer.jsp" />
    </body>
</html>
