/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Stuart
 */
public class DBConnect {

    private String dbName;
    private Connection conn;
    private PreparedStatement pstmt;
    private ResultSet rset;

    public DBConnect() {
        dbName = "PropertyRecords";
    }

    public void createConnection() throws SQLException, ClassNotFoundException {
        String userId = "prdcd";
        String password = "4Bigtittycowgirl!";

        try {
            Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
            conn = DriverManager.getConnection("jdbc:oracle:thin:@tom.uopnet.plymouth.ac.uk:1521:orcl",
                    userId, password);
            System.out.println("Connection OK");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void closeConnection() throws SQLException, ClassNotFoundException {
        conn.close();
    }

    public PropertyList allProperties() throws SQLException, ClassNotFoundException {
        String queryString;
        PropertyList properties;

        queryString = "SELECT * FROM PROPERTY";
        pstmt = conn.prepareStatement(queryString);
        rset = pstmt.executeQuery();

        properties = new PropertyList();
        while (rset.next()) {
            properties.addProperty(new Property(Integer.parseInt(rset.getString(1)), Integer.parseInt(rset.getString(2)), rset.getString(3), Integer.parseInt(rset.getString(4)), Integer.parseInt(rset.getString(5)), Integer.parseInt(rset.getString(6)), rset.getString(7), rset.getString(8), Integer.parseInt(rset.getString(9)), rset.getString(10), rset.getString(11), rset.getString(12), rset.getString(13)));
        }

        return properties;
    }
}
